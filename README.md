# Skeletron - NodeMCU / ESP8266 NeoPixel Control over WiFi #

Skeletron is a project demonstrating ESP8266 Wifi connectivity and control of a NeoPixel LED array through ESP Web Server


# Skeletron? #

* My daughter painted a ceramic skeleton and we wanted its eyes to light up, and the ability to control the lights from a web browser / tablet etc.

![skeletron-sm.jpg](https://bitbucket.org/repo/BRbR7a/images/2948043724-skeletron-sm.jpg)


Here's the interface in a browser / tablet etc...


![skeletron_screenshot.png](https://bitbucket.org/repo/BRbR7a/images/1298120080-skeletron_screenshot.png)

### How do I get set up? ###

* Get a NodeMCU device - it's a little more expensive than raw ESP8266 but has the benefit of a built in USB Serial port and many exposed GPIO pins.   
* Configuration
* Dependencies:  Uses several of the Arduino ESP libraries - WebServer, NeoPixelBus, etc
* Environment:  I am using PlatformIO on OSX

![skeletron.png](https://bitbucket.org/repo/BRbR7a/images/3903586658-skeletron.png)


![nodemcu.png](https://bitbucket.org/repo/BRbR7a/images/1242224607-nodemcu.png)